from abc import ABC, abstractmethod
from src.presenter.helper.http_models import HttpRequest, HttpResponse


class IRoute(ABC):
    ''' '''

    @abstractmethod
    def handle(self, request: HttpRequest) -> HttpResponse:
        ''' '''
        pass

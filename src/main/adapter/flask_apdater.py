from src.presenter.error.http_error import HTTPError
from flask import Request
from src.presenter.helper import HttpRequest, HttpResponse
from src.main.interface.IRoute import IRoute


class FaskAdapter:
    '''convert flask objects'''

    def execute(
        self, flask_request: Request, controller: IRoute
    ) -> HttpResponse:
        ''' '''
        request = HttpRequest(body=flask_request.json)
        try:
            response = controller.handle(request)
        except Exception as e:
            message = str(e)
            response = HttpResponse(
                body={'message': message}, status_code=HTTPError._400.code
            )
        return response

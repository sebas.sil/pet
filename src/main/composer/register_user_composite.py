from src.main.interface import IRoute
from src.presenter.controller import RegisterUserController
from src.data.use_case import UserRegistration
from src.infra.repository import UserRepository
from src.infra.db_config import DBConnectionHandler
from src.infra.run_config import MigrateDB


def registerUserComposite() -> IRoute:
    ''' '''
    connection = DBConnectionHandler()
    repository = UserRepository(connection)
    use_case = UserRegistration(repository)
    controller = RegisterUserController(use_case)

    return controller


def setup():
    ''' '''
    MigrateDB()

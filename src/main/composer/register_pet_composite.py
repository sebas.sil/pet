from src.main.interface import IRoute
from src.presenter.controller import RegisterPetController
from src.data.use_case import PetRegistration
from src.infra.repository import PetRepository
from src.infra.db_config import DBConnectionHandler


def registerPetComposite() -> IRoute:
    ''' '''
    connection = DBConnectionHandler()
    repository = PetRepository(connection)
    use_case = PetRegistration(repository)
    controller = RegisterPetController(use_case)

    return controller

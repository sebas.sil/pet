from flask import Blueprint, jsonify, request
from src.main.composer import registerUserComposite, setup
from src.main.composer import registerPetComposite
from src.main.adapter import FaskAdapter

api_route_bp = Blueprint('api_routes', __name__)


@api_route_bp.route('/setup', methods=['POST'])
def create_env():
    ''' '''
    adapter = FaskAdapter()
    adapter.execute(request, setup())
    return jsonify({'status': 'ok'})


@api_route_bp.route('/user', methods=['POST'])
def register_user():
    ''' '''
    adapter = FaskAdapter()
    response = adapter.execute(request, registerUserComposite())
    return jsonify(
        {
            'status': response.status_code,
            'data': {
                'id': response.body.id,
                'name': response.body.name,
            },
        }
    )


@api_route_bp.route('/pet', methods=['POST'])
def register_pet():
    ''' '''
    adapter = FaskAdapter()
    response = adapter.execute(request, registerPetComposite())
    return jsonify(
        {
            'status': response.status_code,
            'data': {
                'id': response.body.id,
                'name': response.body.name,
                'type': response.body.type,
                'age': response.body.age,
                'owner': {'id': response.body.user_id},
            },
        }
    )

from .db_config import Base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .entities import *
from src.infra.db_config import DBConnectionHandler


class MigrateDB(DBConnectionHandler):
    ''' '''

    def __init__(self, connection_string: str = 'sqlite:///database.db3'):
        super().__init__(connection_string)
        Base.metadata.create_all(super().get_engine())

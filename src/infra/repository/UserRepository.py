from src.infra.db_config import DBConnectionHandler
from src.infra.entities.UserEntitySchema import UserEntitySchema
from src.domain.models import User
from typing import List
from src.data.interface.IUserRepository import IUserRepository


class UserRepository(IUserRepository):
    '''user repository'''

    def __init__(self, connection: DBConnectionHandler):
        ''' '''
        self.__connection = connection

    def insert(self, name: str, password: str) -> User:
        '''persists an user
        :param - name: username
               - password: user password
        :return - User or Exception
        '''

        with self.__connection as conn:
            try:
                user = UserEntitySchema(name=name, password=password)

                conn.session.add(user)
                conn.session.commit()
                return User(user.id, user.name, password)
            except Exception:
                conn.session.rollback()
                raise
            finally:
                conn.session.close()

    def select(self, id: int = None, name: str = None) -> List[User]:
        '''get users
        :param - id: user unique id
               - name: username
        :return - List of User or Exception
        '''

        with self.__connection as conn:

            query = conn.session.query(UserEntitySchema)
            if id:
                query.filter_by(id=id)

            if name:
                query.filter_by(name=name)

            try:
                return query.all()
            except Exception:
                conn.session.rollback()
                raise
            finally:
                conn.session.close()

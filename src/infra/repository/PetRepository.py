from src.infra.db_config import DBConnectionHandler
from src.infra.entities.PetEntitySchema import PetEntitySchema
from src.domain.models import Pet, AnimalType
from typing import List
from src.data.interface.IPetRepository import IPetRepository


class PetRepository(IPetRepository):
    '''pet repository'''

    def __init__(self, connection: DBConnectionHandler):
        ''' '''
        self.__connection = connection

    def insert(
        self, name: str, type: AnimalType, age: int, user_id: int
    ) -> Pet:
        '''persists an user
        :param - name: pet name
               - type: pet type
               - age: pet age
               - user_id: owner's pet id
        :return - Pet or Exception
        '''

        with self.__connection as conn:
            try:
                pet = PetEntitySchema(
                    name=name, type=type, age=age, user_id=user_id
                )

                conn.session.add(pet)
                conn.session.commit()
                return Pet(pet.id, pet.name, pet.type, pet.age, pet.user_id)
            except Exception:
                conn.session.rollback()
                raise
            finally:
                conn.session.close()

    def select(
        self,
        id: int = None,
        name: str = None,
        type: AnimalType = None,
        age: int = None,
        user_id: int = None,
    ) -> List[Pet]:
        '''get users
        :param - id: pet unique id
               - name: pet name
               - type: pet type
               - age: pet age
               - user_id: owner's pet id
        :return - List of Pet or Exception
        '''

        with self.__connection as conn:

            query = conn.session.query(PetEntitySchema)
            if id:
                query.filter_by(id=id)

            if name:
                query.filter_by(name=name)

            if type:
                query.filter_by(type=type.value)

            if age:
                query.filter_by(age=age)

            if user_id:
                query.filter_by(user_id=user_id)

            try:
                return query.all()
            except Exception:
                conn.session.rollback()
                raise
            finally:
                conn.session.close()

from src.infra.repository.PetRepository import PetRepository
from src.domain.models import AnimalType
import pytest
from src.infra.run_config import MigrateDB


# in conftest.py
@pytest.fixture(autouse=True)
def init_db():
    pytest.migration = MigrateDB('sqlite:///:memory:')
    yield  # this is where the testing happens


def test_insert():
    ''' '''

    name: str = 'Mimo'
    type: AnimalType = AnimalType.CAT
    age: int = 7
    user_id = 1
    repo = PetRepository(pytest.migration)
    pet = repo.insert(name, type, age, user_id)

    assert pet.id == 1
    assert pet.name == name
    assert pet.age == age
    assert pet.user_id == user_id


def test_select():
    ''' '''

    name: str = 'Mimo'
    type: AnimalType = AnimalType.CAT
    age: int = 7
    user_id = 1
    repo = PetRepository(pytest.migration)
    pet = repo.insert(name, type, age, user_id)

    pets1 = repo.select(id=id)
    pets2 = repo.select(name=name)
    pets3 = repo.select(type=type)
    pets4 = repo.select(age=age)
    pets5 = repo.select(user_id=user_id)
    pets6 = repo.select(id=id, name=name, type=type, age=age, user_id=user_id)

    assert pet in pets1
    assert pet in pets2
    assert pet in pets3
    assert pet in pets4
    assert pet in pets5
    assert pet in pets6

    assert len(pets1) == 1
    assert len(pets2) == 1
    assert len(pets3) == 1
    assert len(pets4) == 1
    assert len(pets5) == 1
    assert len(pets6) == 1

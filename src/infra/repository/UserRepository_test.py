from src.infra.repository import UserRepository
import pytest
from src.infra.run_config import MigrateDB


# in conftest.py
@pytest.fixture(autouse=True)
def init_db():
    pytest.migration = MigrateDB('sqlite:///:memory:')
    yield  # this is where the testing happens


def test_insert():
    ''' '''

    name: str = 'fabio.silveira'
    password: str = 'Welcome1'
    repo = UserRepository(pytest.migration)
    user = repo.insert(name, password)

    assert user.id == 1
    assert user.name == name
    assert user.password == password


def test_select():
    ''' '''

    name: str = 'fabio.silveira'
    password: str = 'Welcome1'
    repo = UserRepository(pytest.migration)
    user = repo.insert(name, password)

    users1 = repo.select(name=name)

    users2 = repo.select(id=id)

    users3 = repo.select(id=id, name=name)

    assert user in users1
    assert user in users2
    assert user in users3

    assert len(users1) == 1
    assert len(users2) == 1
    assert len(users3) == 1

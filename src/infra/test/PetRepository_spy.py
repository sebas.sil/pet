from src.data.interface import IPetRepository
from src.domain.models import Pet, AnimalType
from typing import List


class PetRepositorySpy(IPetRepository):
    ''' '''

    def __init__(self):
        self.insert_params = {}
        self.select_params = {}

    def insert(
        self, name: str, type: AnimalType, age: int, user_id: int
    ) -> Pet:
        self.insert_params['name'] = name
        self.insert_params['type'] = type
        self.insert_params['age'] = age
        self.insert_params['user_id'] = user_id

        return Pet(0, 'Mimo', AnimalType.CAT, 1, 0)

    def select(
        self,
        id: int = None,
        name: str = None,
        type: AnimalType = None,
        age: int = None,
        user_id: int = None,
    ) -> List[Pet]:
        self.select_params['id'] = id
        self.select_params['name'] = name
        self.select_params['type'] = type
        self.select_params['age'] = age
        self.select_params['user_id'] = user_id

        return [Pet(0, 'Mimo', AnimalType.CAT, 1, 0)]

from src.data.interface import IUserRepository
from src.domain.models import User
from typing import List


class UserRepositorySpy(IUserRepository):
    ''' '''

    def __init__(self):
        self.insert_params = {}
        self.select_params = {}

    def insert(self, name: str, password: str) -> User:
        self.insert_params['name'] = name
        self.insert_params['password'] = password

        return User(0, 'Fabio', 'Welcome1')

    def select(self, id: int = None, name: str = None) -> List[User]:
        self.select_params['id'] = id
        self.select_params['name'] = name

        return [User(0, 'Fabio', 'Welcome1')]

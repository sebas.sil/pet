from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class DBConnectionHandler:
    """sqlalchemy database connection"""

    def __init__(self, connection_string: str = 'sqlite:///database.db3'):
        """ """
        self.__connection_string = connection_string
        self.__engine = None
        self.session = None

    def get_engine(self):
        """ """
        if not self.__engine:
            self.__engine = create_engine(self.__connection_string)
        return self.__engine

    def __enter__(self):
        """ """
        if not self.__engine:
            self.__engine = create_engine(self.__connection_string)
        if not self.session:
            session_maker = sessionmaker()
            self.session = session_maker(bind=self.__engine)
        return self

    def __exit__(self, ex_type, ex_val, ex_track):
        """ """
        if self.session:
            self.session.close()

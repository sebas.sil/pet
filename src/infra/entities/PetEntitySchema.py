import enum
from sqlalchemy import Column, String, Integer, ForeignKey, Enum, SmallInteger
from src.infra.db_config import Base


class AnimalType(str, enum.Enum):
    """define pets types"""

    DOG: str = 'DOG'
    CAT: str = 'CAT'
    FISH: str = 'FISH'
    TURTLE: str = 'TURTLE'

    def __repr__(self) -> str:
        return self.value


class PetEntitySchema(Base):
    """An database table representation.
    It's not a clean code Entity representation"""

    __tablename__ = 'pet'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    type = Column(Enum(AnimalType), nullable=False)
    age = Column(SmallInteger)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)

    def __rep__(self):
        return f'pet [id={self.id} type={self.type} \
                name={self.name} user={self.user_id}]'

    def __eq__(self, other):
        return (
            self.id == other.id
            and self.name == other.name
            and self.type == other.type
            and self.age == other.age
            and self.user_id == other.user_id
        )

from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship
from src.infra.db_config import Base
from . import PetEntitySchema


class UserEntitySchema(Base):
    """An database table representation.
    It's not a clean code Entity representation"""

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(50), nullable=False)
    password = Column(String(50), nullable=False)
    id_pet = relationship(PetEntitySchema)

    def __rep__(self):
        return f'user [id={self.id} name={self.name}]'

    def __eq__(self, other):
        return (
            self.id == other.id
            and self.name == other.name
            and self.password == other.password
        )

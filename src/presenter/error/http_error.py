from enum import Enum


class HTTPError(int, Enum):
    ''' '''

    def __new__(cls, *args, **kwds):
        value = len(cls.__members__) + 1
        obj = int.__new__(cls)
        obj._value_ = value
        return obj

    def __init__(self, code, label):
        self.code = code
        self.label = label

    # https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Status/422
    _422 = (422, 'Unprocessable Entity')
    # https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Status/400
    _400 = (400, 'Bad Request')
    # https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Status/405
    _405 = (405, 'Method Not Allowed')

from typing import Dict


class HttpRequest:
    '''HTTP Request representation'''

    def __init__(
        self, header: Dict = None, body: Dict = None, query: Dict = None
    ):
        ''' '''
        self.header = header
        self.body = body
        self.query = query

    def __repr__(self):
        return f'header={self.header}, body={self.body}, query={self.query}'


class HttpResponse:
    '''HTTP Response representation'''

    def __init__(
        self, header: Dict = None, body: any = None, status_code: int = None
    ):
        ''' '''
        self.header = header
        self.body = body
        self.status_code = status_code

    def __repr__(self):
        return f'header={self.header}, \
            body={self.body}, status_code={self.status_code}'

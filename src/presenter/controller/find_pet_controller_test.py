from src.data.test import PetFindSpy
from src.infra.entities.PetEntitySchema import AnimalType
from src.presenter.controller import FindPetController
from src.presenter.helper.http_models import HttpRequest, HttpResponse
from pytest import raises


def test_handle():
    ''' '''
    request: HttpRequest = HttpRequest(
        body={}, header={}, query={'id': 10, 'type': 'CAT'}
    )
    useCase: PetFindSpy = PetFindSpy()
    controller = FindPetController(useCase)
    response: HttpResponse = controller.handle(request)

    assert response.status_code == 200
    assert useCase.find_parameters['id'] == 10
    assert useCase.find_parameters['name'] is None
    assert useCase.find_parameters['age'] is None
    assert useCase.find_parameters['type'] == AnimalType.CAT
    assert useCase.find_parameters['user_id'] is None


def test_handle_exception():
    ''' '''
    request: HttpRequest = HttpRequest(body={}, header={}, query={'id': '10'})
    useCase: PetFindSpy = PetFindSpy()
    controller = FindPetController(useCase)

    with raises(Exception) as e_info:
        controller.handle(request)

    assert str(e_info.value) == str(
        Exception(
            'id, age and user_id must be integers, \
                    name must be string and type must by AnimalType'
        )
    )
    assert useCase.find_parameters == {}

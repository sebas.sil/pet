from src.data.test import PetRegistrationSpy
from src.infra.entities.PetEntitySchema import AnimalType
from src.presenter.controller import RegisterPetController
from src.presenter.helper.http_models import HttpRequest, HttpResponse
import pytest


# in conftest.py
@pytest.fixture(autouse=True)
def init_db():
    pytest.request: HttpRequest = HttpRequest(
        body={'name': 'Mimo', 'age': 2, 'type': 'CAT', 'user_id': 1},
        header={},
        query={},
    )
    pytest.service: PetRegistrationSpy = PetRegistrationSpy()
    pytest.controller = RegisterPetController(pytest.service)
    yield  # this is where the testing happens


def test_handle():
    ''' '''
    response: HttpResponse = pytest.controller.handle(pytest.request)

    assert response.status_code == 200
    assert pytest.service.register_parameters['name'] == 'Mimo'
    assert pytest.service.register_parameters['age'] == 2
    assert pytest.service.register_parameters['type'] == AnimalType.CAT
    assert pytest.service.register_parameters['user_id'] == 1


def test_handle_exception():
    ''' '''
    pytest.request.body['age'] = 'string'
    with pytest.raises(Exception) as e_info:
        pytest.controller.handle(pytest.request)

    assert str(e_info.value) == str(
        Exception(
            'id, age and user_id must be integers, \
                    name must be string and type must by AnimalType'
        )
    )
    assert pytest.service.register_parameters == {}


def test_handle_exception_body():
    ''' '''
    pytest.request.body = {}
    response: HttpResponse = pytest.controller.handle(pytest.request)

    assert response.status_code == 405
    assert response.body == 'Method Not Allowed'
    assert pytest.service.register_parameters == {}

from src.data.test import UserRegistrationSpy
from src.presenter.controller import RegisterUserController
from src.presenter.helper.http_models import HttpRequest, HttpResponse
import pytest


# in conftest.py
@pytest.fixture(autouse=True)
def init_db():
    pytest.request: HttpRequest = HttpRequest(
        body={'username': 'fabio', 'password': 'Welcome1'},
        header={},
        query={},
    )
    pytest.service: UserRegistrationSpy = UserRegistrationSpy()
    pytest.controller = RegisterUserController(pytest.service)
    yield  # this is where the testing happens


def test_handle():
    ''' '''
    response: HttpResponse = pytest.controller.handle(pytest.request)

    assert response.status_code == 200
    assert pytest.service.register_parameters['username'] == 'fabio'
    assert pytest.service.register_parameters['password'] == 'Welcome1'


def test_handle_exception():
    ''' '''
    pytest.request.body['username'] = 1
    with pytest.raises(Exception) as e_info:
        pytest.controller.handle(pytest.request)

    assert str(e_info.value) == str(
        Exception('username and password must be strings')
    )
    assert pytest.service.register_parameters == {}


def test_handle_exception_body():
    ''' '''
    pytest.request.body = {}
    response: HttpResponse = pytest.controller.handle(pytest.request)

    assert response.status_code == 405
    assert response.body == 'Method Not Allowed'
    assert pytest.service.register_parameters == {}

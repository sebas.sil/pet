from src.data.test import UserFindSpy
from src.presenter.controller.find_user_controller import FindUserController
from src.presenter.helper.http_models import HttpRequest, HttpResponse
from src.domain.models import User
from pytest import raises


def test_handle():
    ''' '''
    request: HttpRequest = HttpRequest(
        body={}, header={}, query={'id': 10, 'username': 'Fabio'}
    )
    useCase: UserFindSpy = UserFindSpy()
    controller = FindUserController(useCase)
    response: HttpResponse = controller.handle(request)

    assert response.status_code == 200
    assert useCase.find_parameters['id'] == 10
    assert useCase.find_parameters['username'] == 'Fabio'
    assert response.body == [User(0, 'Fabio', 'Welcome1')]


def test_handle_exception():
    ''' '''
    request: HttpRequest = HttpRequest(body={}, header={}, query={'id': '10'})
    useCase: UserFindSpy = UserFindSpy()
    controller = FindUserController(useCase)

    with raises(Exception) as e_info:
        controller.handle(request)

    assert str(e_info.value) == str(
        Exception('id must by integer and password must be string')
    )
    assert useCase.find_parameters == {}

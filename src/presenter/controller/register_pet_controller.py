from src.domain.use_case import IPetRegistration
from src.main.interface.IRoute import IRoute
from src.presenter.helper.http_models import HttpRequest, HttpResponse
from src.presenter.error import HTTPError
from src.domain.models import AnimalType


class RegisterPetController(IRoute):
    '''define controller to register pet use case'''

    def __init__(self, findSerice: IPetRegistration):
        ''' '''
        self.__serice = findSerice

    def handle(self, httpRequest: HttpRequest) -> HttpResponse:
        ''' '''
        response: HttpResponse = HttpResponse(
            body=HTTPError._422.label, status_code=HTTPError._422.code
        )
        if httpRequest.body:
            '''POST'''
            age = httpRequest.body.get('age')
            name = httpRequest.body.get('name')
            type = (
                AnimalType[httpRequest.body.get('type')]
                if httpRequest.body.get('type')
                else None
            )
            user_id = httpRequest.body.get('user_id')

            pet = self.__serice.register(name, type, age, user_id)
            response.body = pet
            response.status_code = 200

        else:
            response: HttpResponse = HttpResponse(
                body=HTTPError._405.label, status_code=HTTPError._405.code
            )

        return response

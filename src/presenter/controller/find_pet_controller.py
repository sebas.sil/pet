from src.domain.use_case import IPetFind
from src.main.interface.IRoute import IRoute
from src.presenter.helper.http_models import HttpRequest, HttpResponse
from src.presenter.error import HTTPError
from src.domain.models import AnimalType


class FindPetController(IRoute):
    '''define controller to find user use case'''

    def __init__(self, findSerice: IPetFind):
        ''' '''
        self.__findSerice = findSerice

    def handle(self, httpRequest: HttpRequest) -> HttpResponse:
        ''' '''

        response: HttpResponse = HttpResponse(
            body=HTTPError._422.label, status_code=HTTPError._422.code
        )
        if httpRequest.body == {}:
            '''GET'''
            id = httpRequest.query.get('id')
            age = httpRequest.query.get('age')
            name = httpRequest.query.get('name')
            type = (
                AnimalType[httpRequest.query.get('type')]
                if httpRequest.query.get('type')
                else None
            )
            user_id = httpRequest.query.get('user_id')

            pets = self.__findSerice.find(id, name, type, age, user_id)
            response.body = pets
            response.status_code = 200
        else:
            response = HttpResponse(HTTPError.http405())

        return response

from src.domain.use_case import IUserRegistration
from src.main.interface.IRoute import IRoute
from src.presenter.helper.http_models import HttpRequest, HttpResponse
from src.presenter.error import HTTPError


class RegisterUserController(IRoute):
    ''' '''

    def __init__(self, service: IUserRegistration):
        ''' '''
        self.__service = service

    def handle(self, httpRequest: HttpRequest) -> HttpResponse:
        ''' '''
        response: HttpResponse = HttpResponse(
            body=HTTPError._422.label, status_code=HTTPError._422.code
        )
        if httpRequest.body:
            '''POST'''
            username = httpRequest.body.get('username')
            password = httpRequest.body.get('password')
            user = self.__service.register(username, password)
            response.body = user
            response.status_code = 200
        else:
            response: HttpResponse = HttpResponse(
                body=HTTPError._405.label, status_code=HTTPError._405.code
            )

        return response

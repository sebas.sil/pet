from src.domain.use_case import IUserFind
from src.main.interface.IRoute import IRoute
from src.presenter.helper.http_models import HttpRequest, HttpResponse
from src.presenter.error import HTTPError


class FindUserController(IRoute):
    '''define controller to find pet use case'''

    def __init__(self, findSerice: IUserFind):
        ''' '''
        self.__findSerice = findSerice

    def handle(self, httpRequest: HttpRequest) -> HttpResponse:
        ''' '''

        response: HttpResponse = HttpResponse(
            body=HTTPError._422.label, status_code=HTTPError._422.code
        )
        if httpRequest.body == {}:
            '''GET'''
            id = httpRequest.query.get('id')
            name = httpRequest.query.get('username')

            users = self.__findSerice.find(id, name)
            response.body = users
            response.status_code = 200
        else:
            response = HttpResponse(HTTPError.http405())

        return response

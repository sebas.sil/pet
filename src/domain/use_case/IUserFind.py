from abc import ABC, abstractmethod
from src.domain.models import User
from typing import List


class IUserFind(ABC):
    ''' '''

    @abstractmethod
    def find(self, id: int, username: str) -> List[User]:
        pass

from abc import ABC, abstractmethod
from src.domain.models import Pet, AnimalType


class IPetRegistration(ABC):
    ''' '''

    @abstractmethod
    def register(
        self, name: str, type: AnimalType, age: int, user_id: int
    ) -> Pet:
        pass

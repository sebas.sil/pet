from abc import ABC, abstractmethod
from src.domain.models import User


class IUserRegistration(ABC):
    ''' '''

    @abstractmethod
    def register(self, username: str, userpassword: str) -> User:
        pass

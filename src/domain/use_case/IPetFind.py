from abc import ABC, abstractmethod
from src.domain.models import Pet, AnimalType
from typing import List


class IPetFind(ABC):
    ''' '''

    @abstractmethod
    def find(
        self, id: int, name: str, type: AnimalType, age: int, user_id: int
    ) -> List[Pet]:
        pass

from src.infra.entities.PetEntitySchema import AnimalType
from collections import namedtuple


Pet = namedtuple('Pet', ['id', 'name', 'type', 'age', 'user_id'])
User = namedtuple('User', ['id', 'name', 'password'])

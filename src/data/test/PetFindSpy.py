from src.domain.use_case import IPetFind
from src.domain.models import Pet, AnimalType
from typing import List


class PetFindSpy(IPetFind):
    ''' '''

    def __init__(self):
        self.find_parameters = {}

    def find(
        self,
        id: int = None,
        name: str = None,
        type: AnimalType = None,
        user_id: int = None,
        age: int = None,
    ) -> List[Pet]:

        if (
            (isinstance(id, int) or id is None)
            and (isinstance(name, str) or name is None)
            and (isinstance(type, AnimalType) or type is None)
            and (isinstance(age, int) or age is None)
            and (isinstance(user_id, int) or user_id is None)
        ):
            self.find_parameters['id'] = id
            self.find_parameters['name'] = name
            self.find_parameters['type'] = type
            self.find_parameters['age'] = age
            self.find_parameters['user_id'] = user_id
            return [Pet(0, 'Mimo', AnimalType.CAT, 1, 0)]

        else:
            raise Exception(
                'id, age and user_id must be integers, \
                    name must be string and type must by AnimalType'
            )

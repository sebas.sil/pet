from src.domain.use_case import IUserFind
from src.domain.models import User
from typing import List


class UserFindSpy(IUserFind):
    ''' '''

    def __init__(self):
        self.find_parameters = {}

    def find(self, id: int, username: str) -> List[User]:
        if isinstance(id, int) and isinstance(username, str):
            self.find_parameters['id'] = id
            self.find_parameters['username'] = username
            return [User(0, 'Fabio', 'Welcome1')]

        else:
            raise Exception('id must by integer and password must be string')

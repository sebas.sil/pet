from src.domain.use_case import IPetRegistration
from src.domain.models import Pet, AnimalType


class PetRegistrationSpy(IPetRegistration):
    ''' '''

    def __init__(self):
        self.register_parameters = {}

    def register(
        self, name: str, type: AnimalType, age: int, user_id: int
    ) -> Pet:

        if (
            (isinstance(name, str) or name is None)
            and (isinstance(type, AnimalType) or type is None)
            and (isinstance(age, int) or age is None)
            and (isinstance(user_id, int) or user_id is None)
        ):
            self.register_parameters['name'] = name
            self.register_parameters['type'] = type
            self.register_parameters['age'] = age
            self.register_parameters['user_id'] = user_id
            return Pet(0, 'Mimo', AnimalType.CAT, 1, 0)

        else:
            raise Exception(
                'id, age and user_id must be integers, \
                    name must be string and type must by AnimalType'
            )

from src.domain.use_case import IUserRegistration
from src.domain.models import User


class UserRegistrationSpy(IUserRegistration):
    ''' '''

    def __init__(self):
        self.register_parameters = {}

    def register(self, username: str, userpassword: str) -> User:
        if isinstance(username, str) and isinstance(userpassword, str):
            self.register_parameters['username'] = username
            self.register_parameters['password'] = userpassword
            return User(0, 'Fabio', 'Welcome1!')
        else:
            raise Exception('username and password must be strings')

from src.domain.use_case import IUserRegistration
from src.domain.models import User
from src.data.interface import IUserRepository


class UserRegistration(IUserRegistration):
    ''' '''

    def __init__(self, repository: IUserRepository):
        self.__repository = repository

    def register(self, username: str, userpassword: str) -> User:
        if isinstance(username, str) and isinstance(userpassword, str):
            return self.__repository.insert(
                name=username, password=userpassword
            )
        else:
            raise Exception('username and password must be strings')

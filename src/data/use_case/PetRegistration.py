from src.domain.use_case import IPetRegistration
from src.domain.models import Pet, AnimalType
from src.data.interface import IPetRepository


class PetRegistration(IPetRegistration):
    ''' '''

    def __init__(self, repository: IPetRepository):
        self.__repository = repository

    def register(
        self, name: str, type: AnimalType, user_id: int, age: int = None
    ) -> Pet:
        if (
            isinstance(name, str)
            and isinstance(type, AnimalType)
            and (isinstance(age, int) or age is None)
            and isinstance(user_id, int)
        ):
            return self.__repository.insert(name, type, age, user_id)
        else:
            raise Exception(
                'age and user_id must be integers, \
                    name must be string and type must by AnimalType'
            )

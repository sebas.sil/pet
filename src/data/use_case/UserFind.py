from src.domain.use_case import IUserFind
from src.domain.models import User
from src.data.interface import IUserRepository
from typing import List


class UserFind(IUserFind):
    ''' '''

    def __init__(self, repository: IUserRepository):
        self.__repository = repository

    def find(self, id: int = None, username: str = None) -> List[User]:
        if isinstance(id, int) and isinstance(username, str):
            return self.__repository.select(id, username)
        else:
            raise Exception('id must by integer and password must be string')

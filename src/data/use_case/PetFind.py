from src.domain.use_case import IPetFind
from src.domain.models import Pet, AnimalType
from src.data.interface import IPetRepository
from typing import List


class PetFind(IPetFind):
    ''' '''

    def __init__(self, repository: IPetRepository):
        self.__repository = repository

    def find(
        self,
        id: int = None,
        name: str = None,
        type: AnimalType = None,
        user_id: int = None,
        age: int = None,
    ) -> List[Pet]:
        if (
            (isinstance(id, int) or id is None)
            and (isinstance(name, str) or name is None)
            and (isinstance(type, AnimalType) or type is None)
            and (isinstance(age, int) or age is None)
            and (isinstance(user_id, int) or user_id is None)
        ):
            return self.__repository.select(id, name, type, age, user_id)
        else:
            raise Exception(
                'id, age and user_id must be integers, \
                    name must be string and type must by AnimalType'
            )

from pytest import raises
from src.data.use_case import UserFind
from src.infra.test import UserRepositorySpy


def test_find():
    ''' '''
    id = 1
    username = 'Fabio Silveira'
    repository = UserRepositorySpy()
    registration = UserFind(repository)
    users = registration.find(id, username)

    user = users[0]
    assert len(users) == 1
    assert user.id == 0
    assert user.name == 'Fabio'
    assert user.password == 'Welcome1'


def test_find_fail():
    ''' '''
    id = 'string'
    repository = UserRepositorySpy()
    registration = UserFind(repository)
    with raises(Exception) as e_info:
        registration.find(id=id)

    assert str(e_info.value) == str(
        Exception('id must by integer and password must be string')
    )
    assert repository.select_params == {}

from pytest import raises
from src.data.use_case import PetRegistration
from src.infra.entities.PetEntitySchema import AnimalType
from src.infra.test import PetRepositorySpy


def test_register():
    ''' '''
    name = 'Mimo'
    type = AnimalType.CAT
    age = 3
    user_id = 1
    repository = PetRepositorySpy()
    registration = PetRegistration(repository)
    pet = registration.register(name, type, age, user_id)

    assert pet.id == 0
    assert pet.name == 'Mimo'
    assert pet.type == AnimalType.CAT
    assert pet.user_id == 0


def test_register_fail():
    ''' '''
    name = 123
    repository = PetRepositorySpy()
    registration = PetRegistration(repository)
    with raises(Exception) as e_info:
        registration.register(name=name, type=AnimalType.CAT, user_id=0)

    assert str(e_info.value) == str(
        Exception(
            'age and user_id must be integers, \
                    name must be string and type must by AnimalType'
        )
    )
    assert repository.insert_params == {}

from pytest import raises
from src.data.use_case import UserRegistration
from src.infra.test import UserRepositorySpy


def test_register():
    ''' '''
    username = 'Fabio Silveira'
    userpassword = 'Welcome123'
    repository = UserRepositorySpy()
    registration = UserRegistration(repository)
    user = registration.register(username, userpassword)

    assert user.id == 0
    assert user.name == 'Fabio'
    assert user.password == 'Welcome1'


def test_register_fail():
    ''' '''
    username = 123
    userpassword = 'Welcome123'
    repository = UserRepositorySpy()
    registration = UserRegistration(repository)
    with raises(Exception) as e_info:
        registration.register(username, userpassword)

    assert str(e_info.value) == str(
        Exception('username and password must be strings')
    )
    assert repository.insert_params == {}

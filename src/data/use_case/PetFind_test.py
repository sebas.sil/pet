from pytest import raises
from src.data.use_case import PetFind
from src.infra.entities.PetEntitySchema import AnimalType
from src.infra.test import PetRepositorySpy


def test_find():
    ''' '''
    id = 1
    name = 'Mimo'
    repository = PetRepositorySpy()
    registration = PetFind(repository)
    pets = registration.find(id, name)

    pet = pets[0]
    assert len(pets) == 1
    assert pet.id == 0
    assert pet.name == 'Mimo'
    assert pet.type == AnimalType.CAT
    assert pet.age == 1
    assert pet.user_id == 0


def test_find_fail():
    ''' '''
    id = 'string'
    repository = PetRepositorySpy()
    registration = PetFind(repository)
    with raises(Exception) as e_info:
        registration.find(id=id)

    assert str(e_info.value) == str(
        Exception(
            'id, age and user_id must be integers, \
                    name must be string and type must by AnimalType'
        )
    )
    assert repository.select_params == {}

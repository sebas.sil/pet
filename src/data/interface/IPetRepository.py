from abc import ABC, abstractmethod
from src.domain.models import Pet, AnimalType
from typing import List


class IPetRepository(ABC):
    ''' '''

    @abstractmethod
    def insert(name: str, type: AnimalType, age: int, user_id: int) -> Pet:
        pass

    def select(
        id: int,
        name: str,
        type: AnimalType,
        age: int,
        user_id: int,
    ) -> List[Pet]:
        pass

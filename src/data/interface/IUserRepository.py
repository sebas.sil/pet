from abc import ABC, abstractmethod
from src.domain.models import User
from typing import List


class IUserRepository(ABC):
    ''' '''

    @abstractmethod
    def insert(self, name: str, password: str) -> User:
        ''' '''
        pass

    @abstractmethod
    def select(self, id: int = None, name: str = None) -> List[User]:
        ''' '''
        pass
